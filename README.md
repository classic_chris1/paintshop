# PaintShop #

Thie application is an iOS implementation of the PaintShop exercise described below written in Swift.

### How to run the application ###

The application requires a filepath or filepaths to be passed as arguments on launch. This can be configured by editing the PaintShop build scheme, specifically the **Arguments Passed On Launch** section.

You will find example values already set for clarity. There are sample files available in the root of the project under the *sample_inputs* folder.

### What to expect from the app ###

The app will first show you a list with all of the filepath specified as arguments.

Selecting any path will initiate a transition to a screen in which the current values of the problem are shown along with a button to mix the paints.

The result will be shown graphically on screen as well as printed to the command line.

### What to expect from the code ###

The logic to solve the problem as specified in the exercise can be found in the *PaintShop.swift* file.

The scenes that make up the app are defined under the *Scenes* group and are implemented using an MVVM structure.


### The original exericse ###
The Challenge
You run a paint shop, and there are a few different colors of paint you can prepare.
Each color can be either "gloss" or "matte".
You have a number of customers, and each have some colors they like, either gloss
or matte. No customer will like more than one color in matte.
You want to mix the colors, so that:
* There is just one batch for each color, and it's either gloss or matte.
* For each customer, there is at least one color they like.
* You make as few mattes as possible (because they are more expensive).
Your program should accept an input file as a command line argument, and print a
result to standard out. An example input file is:
5
1 M 3 G 5 G
2 G 3 M 4 G
5 M
The first line specifies how many colors there are.
Each subsequent line describes a customer. For example, the first customer likes
color 1 in matte, color 3 in gloss and color 5 in gloss.
Your program should read an input file like this, and print out either that it is
impossible to satisfy all the customer, or describe, for each of the colors, whether it
should be made gloss or matte.
The output for the above file should be:
G G G G M
...because all customers can be made happy by every paint being prepared as gloss
except number 5.
An example of a file with no solution is:
1
1 G
1 M
Your program should print
No solution exists
A slightly richer example is:
5
2 M
5 G
1 G
5 G 1 G 4 M
3 G
5 G
3 G 5 G 1 G
3 G
2 M
5 G 1 G
2 M
5 G
4 M
5 G 4 M
...which should print:
G M G M G
One more example. The input:
2
1 G 2 M
1 M
...should produce
M M
There ​is ​no ​time ​limit, ​just ​show ​us ​your ​best ​;-)