//
//  PaintShopViewModelTests.swift
//  PaintShopTests
//
//  Created by Christopher La Pat on 09/01/2018.
//

import XCTest
@testable import PaintShop

class PaintShopViewModelTests: XCTestCase {
    
    var viewModel: PaintShopViewModel!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.viewModel = PaintShopViewModel()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_mixPaints_callback_with_nil_when_no_solution() {
        let expectation = self.expectation(description: "Expect callback parameter to be nil.")
        let inputArray = [
            "2",
            "1 M 2 M",
            "1 M 2 G",
            "1 G"
        ]
        
        self.viewModel.inputData = inputArray
        self.viewModel.paintShop = PaintShop(inputArray: inputArray)
        
        self.viewModel.mixPaints { (mixedPaints) in
            XCTAssertNil(mixedPaints)
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 1.0, handler: nil)
    }
    
    func test_mixPaints_callback_with_result_when_solution() {
        let expectation = self.expectation(description: "Expect callback parameter to be nil.")
        
        let inputArray = [
            "5",
            "1 M 3 G 5 G",
            "2 G 3 M 4 G",
            "5 M"
        ]
        
        self.viewModel.inputData = inputArray
        self.viewModel.paintShop = PaintShop(inputArray: inputArray)
        
        self.viewModel.mixPaints { (mixedPaints) in
            XCTAssertNotNil(mixedPaints)
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 1.0, handler: nil)
    }
    
}
