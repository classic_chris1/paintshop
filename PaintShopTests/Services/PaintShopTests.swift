//
//  PaintShopTests.swift
//  PaintShopTests
//
//  Created by Christopher La Pat on 07/01/2018.
//

import XCTest
@testable import PaintShop

class PaintShopTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_init_returns_nil_when_input_array_empty() {
        let paintShop = PaintShop(inputArray: [])
        
        XCTAssertNil(paintShop)
    }
    
    func test_init_return_nil_when_first_line_not_a_number() {
        let inputArray = [
            "NOT_A_NUMBER",
            "1 M 3 G 5 G",
            "2 G 3 M 4 G",
            "5 M"
        ]
        let paintShop = PaintShop(inputArray: inputArray)
        
        XCTAssertNil(paintShop)
    }
    
    func test_init_return_nil_when_first_line_is_zero() {
        let inputArray = [
            "0",
            "1 M 3 G 5 G",
            "2 G 3 M 4 G",
            "5 M"
        ]
        let paintShop = PaintShop(inputArray: inputArray)
        
        XCTAssertNil(paintShop)
    }
    
    // expects a number followed by a letter, always even number
    func test_init_returns_nil_when_paint_data_format_is_wrong() {
        let inputArrayColorFieldNotAnInteger = [
            "5",
            "A M 3 G 5 G"
        ]
        
        let inputArrayInvalidFormat = [
            "5",
            "1 M 3 5 G"
        ]
        
        let paintShopWrongColorField = PaintShop(inputArray: inputArrayColorFieldNotAnInteger)
        let paintShopInvalidFormat = PaintShop(inputArray: inputArrayInvalidFormat)
        
        XCTAssertNil(paintShopWrongColorField)
        XCTAssertNil(paintShopInvalidFormat)
    }
    
    func test_init_returns_nil_when_finish_is_not_gloss_or_matte() {
        let inputArray = [
            "5",
            "1 A 2 B 3 C"
        ]
        let paintShop = PaintShop(inputArray: inputArray)
        
        XCTAssertNil(paintShop)
    }
    
    func test_paintShop_correctly_created_with_correct_inputs() {
        let inputArray = [
            "5",
            "2 G 3 M",
            "1 M 3 G 5 G",
            "5 M"
        ]
        
        let paintShop = PaintShop(inputArray: inputArray)
        
        XCTAssertNotNil(paintShop)
        XCTAssertEqual(paintShop!.customers.count, 3)
        
        // check customers sorted by number of paint preferences, lowest number first
        XCTAssertEqual(paintShop!.customers[0].paints.count, 1)
        XCTAssertEqual(paintShop!.customers[1].paints.count, 2)
        XCTAssertEqual(paintShop!.customers[2].paints.count, 3)
    }
    
    func test_mixing_paints_with_no_solution_returns_nil() {
        let inputArray = [
            "2",
            "1 M 2 M",
            "1 M 2 G",
            "1 G"
        ]
        
        let paintShop = PaintShop(inputArray: inputArray)
        let results = paintShop?.mixPaints()
        
        XCTAssertNil(results)
    }
    
    func test_mixing_paints_with_solution() {
        let inputArray = [
            "5",
            "1 M 3 G 5 G",
            "2 G 3 M 4 G",
            "5 M"
        ]
        
        let expectedResults = [
            "G",
            "G",
            "G",
            "G",
            "M"
        ]
        
        let inputArray1 = [
            "5",
            "2 M",
            "5 G",
            "1 G",
            "5 G 1 G 4 M",
            "3 G",
            "5 G",
            "3 G 5 G 1 G",
            "3 G",
            "2 M",
            "5 G 1 G",
            "2 M",
            "5 G",
            "4 M",
            "5 G 4 M"
        ]
        
        let expectedResults1 = [
            "G",
            "M",
            "G",
            "M",
            "G"
        ]
        
        let inputArray2 = [
            "2",
            "1 G 2 M",
            "1 M"
        ]
        
        let expectedResults2 = [
            "M",
            "M"
        ]
        
        let paintShop = PaintShop(inputArray: inputArray)
        let results = paintShop?.mixPaints()
        
        XCTAssertNotNil(results)
        
        let enumeratedResults = results!.enumerated()
        for (index, paint) in enumeratedResults {
            XCTAssertEqual(paint.finish.rawValue, expectedResults[index])
        }
        
        let paintShop1 = PaintShop(inputArray: inputArray1)
        let results1 = paintShop1?.mixPaints()
        
        XCTAssertNotNil(results1)
        
        let enumeratedResults1 = results1!.enumerated()
        for (index, paint) in enumeratedResults1 {
            XCTAssertEqual(paint.finish.rawValue, expectedResults1[index])
        }
        
        let paintShop2 = PaintShop(inputArray: inputArray2)
        let results2 = paintShop2?.mixPaints()
        
        XCTAssertNotNil(results2)
        
        let enumeratedResults2 = results2!.enumerated()
        for (index, paint) in enumeratedResults2 {
            XCTAssertEqual(paint.finish.rawValue, expectedResults2[index])
        }
    }
}
