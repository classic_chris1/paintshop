//
//  Customer.swift
//  PaintShop
//
//  Created by Christopher La Pat on 07/01/2018.
//

import Foundation

struct Customer {
    let paints: [Paint]
}
