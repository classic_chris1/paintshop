//
//  Paint.swift
//  PaintShop
//
//  Created by Christopher La Pat on 07/01/2018.
//

import Foundation

enum PaintFinish: String {
    case gloss = "G"
    case matte = "M"
}

struct Paint {
    let color: Int
    let finish: PaintFinish
    
    func isMatte() -> Bool {
        return self.finish == PaintFinish.matte
    }
}
