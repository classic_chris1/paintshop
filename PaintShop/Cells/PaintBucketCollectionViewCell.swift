//
//  PaintBucketCollectionViewCell.swift
//  PaintShop
//
//  Created by Christopher La Pat on 08/01/2018.
//

import UIKit

class PaintBucketCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    var finish: PaintFinish! {
        didSet {
            switch finish {
            case .gloss:
                self.image.image = UIImage(named: "paint-bucket-glossy")
            case .matte:
                self.image.image = UIImage(named: "paint-bucket-matte")
            default:
                return
            }
        }
    }
}
