//
//  PaintShop.swift
//  PaintShop
//
//  Created by Christopher La Pat on 07/01/2018.
//

import Foundation

enum DataFormatError: Error {
    case illFormedPaint
    case colorNotAnInteger
    case invalidPaintFinish
}

class PaintShop {
    let numberOfPaints: Int
    var customers: [Customer]
    
    init?(inputArray: [String]) {
        guard inputArray.count > 0 else {
            return nil
        }
        
        guard let numPaints = Int(inputArray.first!),
            numPaints > 0 else {
                return nil
        }
        
        self.numberOfPaints = numPaints
        self.customers = []
        
        do {
            self.customers = try self.createCustomers(from: Array(inputArray[1...]))
        } catch {
            return nil
        }
    }
    
    
    func mixPaints() -> [Paint]? {
        var mixedPaints: [Paint?] = [Paint?](repeating: nil, count: self.numberOfPaints)
        
        for customer in self.customers {
            var mattePosibility: Paint? = nil
            var paintChosen = false
            
            for paint in customer.paints {
                let paintIndex = paint.color - 1
                
                if paint.isMatte() {
                    if mixedPaints[paintIndex] == nil {
                        mattePosibility = paint
                        continue
                    }
                    
                    if mixedPaints[paintIndex]!.finish == paint.finish {
                        paintChosen = true
                        break
                    }
                }
                
                if mixedPaints[paintIndex] == nil {
                    mixedPaints[paintIndex] = paint
                    paintChosen = true
                    break
                } else if mixedPaints[paintIndex]!.finish == paint.finish {
                    paintChosen = true
                    break
                } else {
                    continue
                }
            }
            
            if !paintChosen {
                guard let mattePaint = mattePosibility else {
                    return nil // no solution
                }
                
                let mattePaintIndex = mattePaint.color - 1
                mixedPaints[mattePaintIndex] = mattePaint
            }
        }
        
        return self.mixRemainingPaintsAsGlossy(mixedPaints)
    }
    
    private func mixRemainingPaintsAsGlossy(_ mixedPaints: [Paint?]) -> [Paint] {
        let result = mixedPaints.enumerated().map({ (index, paint) -> Paint in
            if paint == nil {
                return Paint(color: index + 1, finish: PaintFinish.gloss)
            }
            
            return paint!
        })
        
        return result
    }
    
    private func createCustomers(from inputArray: [String]) throws -> [Customer] {
        for customerValues in inputArray {
            var customerPaints: [Paint] = []
            let paintPrefs = customerValues.components(separatedBy: .whitespaces)
            
            guard paintPrefs.count % 2 == 0 else {
                throw DataFormatError.illFormedPaint
            }
            
            for i in stride(from: 0, to: paintPrefs.count, by: 2) {
                guard let color = Int(paintPrefs[i]) else {
                    throw DataFormatError.colorNotAnInteger
                }
                
                guard let finish = PaintFinish(rawValue: paintPrefs[i+1]) else {
                    throw DataFormatError.invalidPaintFinish
                }
                
                customerPaints.append(Paint(color: color, finish: finish))
            }
            
            self.customers.append(Customer(paints: customerPaints))
        }
        
        // order customers by number of paint preferences
        self.customers.sort { (c1, c2) -> Bool in
            return c1.paints.count < c2.paints.count
        }
        
        return self.customers
    }
}
