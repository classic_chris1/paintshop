//
//  PaintShopViewModel.swift
//  PaintShop
//
//  Created by Christopher La Pat on 08/01/2018.
//

import Foundation
enum FileError: Error {
    case missingFilePath
    case invalidData
}

protocol PaintShopWorker {
    func loadInputData(filePath: String, callback: (_ error: Error?) -> Void)
    func mixPaints(callback: (_ mixedPaints: [Paint]?) -> Void)
    
    // Text Representation
    func getMixedPaintsAsText() -> String
    
    // TableView data source
    func numberOfSections() -> Int
    func sectionTitle(for section: Int) -> String
    func numberOfItemsToDisplay(in section: Int) -> Int
    func textForItem(at indexPath: IndexPath) -> String
    
    // CollectionView data source
    func numberOfMixedPaints() -> Int
    func paintFinishForCell(at indexPath: IndexPath) -> PaintFinish
}

class PaintShopViewModel: PaintShopWorker {
    var inputData: [String] = []
    var paintShop: PaintShop?
    var mixedPaints: [Paint] = []
    
    func loadInputData(filePath: String, callback: (_ error: Error?) -> Void) {
        self.inputData = self.parseInputFile(at: filePath)
        guard let shop = PaintShop(inputArray: self.inputData) else {
            self.inputData = []
            callback(FileError.invalidData)
            return
        }
        
        self.paintShop = shop
        
        callback(nil)
    }
    
    func mixPaints(callback: (_ mixedPaints: [Paint]?) -> Void) {
        guard let mixedPaints = self.paintShop?.mixPaints() else {
            callback(nil)
            return
        }
        
        self.mixedPaints = mixedPaints
        callback(self.mixedPaints)
    }
    
    func getMixedPaintsAsText() -> String {
        guard self.mixedPaints.count > 0 else {
            return "No solution exists"
        }
        
        var resultString = ""
        for paint in self.mixedPaints {
            resultString = resultString + "\(paint.finish.rawValue) "
        }
        
        return resultString
    }
    
    func numberOfSections() -> Int {
        return 2
    }
    
    func sectionTitle(for section: Int) -> String {
        guard self.inputData.count > 0 else {
            return ""
        }
        
        switch section {
        case 0:
            return "Number of Paints"
        case 1:
            return "Customer Preferences"
        default:
            return ""
        }
    }
    
    func numberOfItemsToDisplay(in section: Int) -> Int {
        guard self.inputData.count > 0 else {
            return 0
        }
        
        switch section {
        case 0:
            return 1
        case 1:
            return self.inputData.count - 1
        default:
            return 0
        }
    }
    
    func textForItem(at indexPath: IndexPath) -> String {
        return self.inputData[indexPath.row + indexPath.section]
    }
    
    func numberOfMixedPaints() -> Int {
        return self.mixedPaints.count
    }
    
    func paintFinishForCell(at indexPath: IndexPath) -> PaintFinish {
        return self.mixedPaints[indexPath.row].finish
    }
    
    private func parseInputFile(at path: String) -> [String] {
        do {
            let fileContents = try String(contentsOfFile: path, encoding: .utf8)
            let inputsArray = fileContents.components(separatedBy: .newlines).filter({ (input) -> Bool in
                return !input.isEmpty
            })
            return inputsArray
        } catch {
            print("Error reading the input file. Please check that the file exists at the path specified.")
            return []
        }
    }
}
