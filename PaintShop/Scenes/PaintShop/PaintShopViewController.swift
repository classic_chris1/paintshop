//
//  PaintShopViewController.swift
//  PaintShop
//
//  Created by Christopher La Pat on 08/01/2018.
//

import UIKit

class PaintShopViewController: UIViewController, UITableViewDataSource, UICollectionViewDataSource {
    
    @IBOutlet weak var inputDataTableView: UITableView!
    @IBOutlet weak var resultsCollectionView: UICollectionView!
    @IBOutlet weak var noSolutionLabel: UILabel!
    @IBOutlet weak var inputDataErrorLabel: UILabel!
    
    var filePath: String!
    var viewModel: PaintShopWorker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.viewModel == nil {
            self.viewModel = PaintShopViewModel()
        }
        
        self.viewModel.loadInputData(filePath: self.filePath) { (error) in
            self.inputDataTableView.tableFooterView = UIView()
            
            guard error == nil else {
                self.inputDataErrorLabel.isHidden = false
                return
            }
            
            self.inputDataTableView.reloadData()
        }
    }
    
    @IBAction func mixPaints(_ sender: UIButton) {
        self.viewModel.mixPaints { (mixedPaints) in
            if mixedPaints == nil {
                self.noSolutionLabel.isHidden = false
            }
            
            print(self.viewModel.getMixedPaintsAsText())
            
            self.resultsCollectionView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfItemsToDisplay(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inputDataCell", for: indexPath)
        cell.textLabel?.text = self.viewModel.textForItem(at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel.sectionTitle(for: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    
    // MARK: - UICollectionViewDataSource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfMixedPaints()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaintCollectionViewCell", for: indexPath) as! PaintBucketCollectionViewCell
        cell.finish = self.viewModel.paintFinishForCell(at: indexPath)
        
        return cell
    }
}
