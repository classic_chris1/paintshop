//
//  InputFileListViewModel.swift
//  PaintShop
//
//  Created by Christopher La Pat on 09/01/2018.
//

import Foundation

protocol UserInputHandling {
    func getFilePaths(_ callback: () -> Void)
    
    func numberOfFiles() -> Int
    func filePath(at row: Int) -> String
    func fileName(at row: Int) -> String
}

class InputFileListViewModel: UserInputHandling {
    var filePaths: [String] = []
    
    func getFilePaths(_ callback: () -> Void) {
        self.filePaths = []
        
        let argCount: Int = Int(CommandLine.argc)
        
        if argCount < 2 {
            callback()
            return
        }

        for i in 1..<argCount {
            self.filePaths.append(CommandLine.arguments[i])
        }
        
        callback()
    }
    
    func numberOfFiles() -> Int {
        return self.filePaths.count
    }
    
    func filePath(at row: Int) -> String {
        return self.filePaths[row]
    }
    
    func fileName(at row: Int) -> String {
        let fileName = self.filePaths[row].split(separator: "/").last
        return fileName != nil ? String(fileName!) : ""
    }
}
