//
//  InputFileListTableViewController.swift
//  PaintShop
//
//  Created by Christopher La Pat on 09/01/2018.
//

import UIKit

class InputFileListTableViewController: UITableViewController {
    var viewModel: UserInputHandling!
    private var selectedFilePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.viewModel == nil {
            self.viewModel = InputFileListViewModel()
        }
        
        self.viewModel.getFilePaths {
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.selectedFilePath = nil
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfFiles()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InputFileTableViewCell", for: indexPath)
        
        cell.textLabel?.text = self.viewModel.fileName(at: indexPath.row)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedFilePath = self.viewModel.filePath(at: indexPath.row)
        self.performSegue(withIdentifier: "ListToPaintShopSegue", sender: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListToPaintShopSegue" {
            let paintShopVC = segue.destination as! PaintShopViewController
            paintShopVC.filePath = self.selectedFilePath
        }
    }
}
